import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class newUserPanel extends JPanel {
	
	private final JTextField idNumber		;
	private final JTextField firstName		;
	private final JTextField lastName		;
	private final JTextField phoneNumber	;
	private final JTextField email			;
	private final JTextField address		;
	private final JTextField city			;
	private final JTextField state			;
	private final JTextField zip			;
	private final JButton    reset			;
	private final JButton    submit		;
	
	public newUserPanel(){
		JPanel mainGrid = new JPanel();//panel that will hold all stuff in tab
		JPanel buttonPanel = new JPanel();//panel for holding buttons
		
		mainGrid.setLayout(new GridBagLayout());//the panel is a grid
		GridBagConstraints c = new GridBagConstraints();//holds properties for cells when added into grid
		
		//1 row, 2 columns, 15 horizontal padding, 0 vertical padding
		buttonPanel.setLayout(new GridLayout(1,2,15,0));
		
		c.insets = new Insets(5, 10, 5, 10);//add some padding around the elements added
		c.anchor = GridBagConstraints.WEST;//align left
		c.fill = GridBagConstraints.HORIZONTAL;
		
		//create labels so the user knows what goes into the text box that will be to the right
		JLabel idNumberLabel = new JLabel("ID Number (Integer) :");
		JLabel firstNameLabel = new JLabel("First Name :");
		JLabel lastNameLabel = new JLabel("Last Name :");
		JLabel phoneNumberLabel = new JLabel("Phone Number :");
		JLabel emailLabel = new JLabel("Email :");
		JLabel addressLabel = new JLabel("Address :");
		JLabel cityLabel = new JLabel("City :");
		JLabel stateLabel = new JLabel("State :");
		JLabel zipLabel = new JLabel("ZIP :");

		
		
		
		//create input boxes for:
		 idNumber = 	new JTextField(25);
		 firstName = 	new JTextField();
		 lastName = 	new JTextField();
		 phoneNumber = 	new JTextField();
		 email = 		new JTextField();
		 address = 		new JTextField();
		 city = 		new JTextField();
		 state = 		new JTextField();
		 zip = 			new JTextField();
		
		reset = new JButton("Reset Fields");//button for reseting input
		submit = new JButton("Submit");

		c.gridx=0;	c.gridy=0;	//set cursor to  first column first row
		
		//add the following labels to grid, move cursor to next row after each insert
		mainGrid.add(idNumberLabel, c); 	c.gridy++;//1
		mainGrid.add(firstNameLabel, c);	c.gridy++;//2
		mainGrid.add(lastNameLabel, c);		c.gridy++;//3
		mainGrid.add(phoneNumberLabel, c);	c.gridy++;//4
		mainGrid.add(emailLabel, c);		c.gridy++;//5
		mainGrid.add(addressLabel, c);		c.gridy++;//6
		mainGrid.add(cityLabel, c);			c.gridy++;//7
		mainGrid.add(stateLabel, c);		c.gridy++;//8
		mainGrid.add(zipLabel, c);			c.gridy++;//9
		
		c.gridx=1;	c.gridy=0;	//set cursor to second column, first row
		
		//add the following text boxes to grid, move cursor to next row after each insert
		mainGrid.add(idNumber, c); 		c.gridy++;//1
		mainGrid.add(firstName, c);		c.gridy++;//2
		mainGrid.add(lastName, c);		c.gridy++;//3
		mainGrid.add(phoneNumber, c);	c.gridy++;//4
		mainGrid.add(email, c);			c.gridy++;//5
		mainGrid.add(address, c);		c.gridy++;//6
		mainGrid.add(city, c);			c.gridy++;//7
		mainGrid.add(state, c);			c.gridy++;//8
		mainGrid.add(zip, c);			c.gridy++;//9
		
		//adding to a GridLayout dosen't require constraints(c), GridBagLayout does
		buttonPanel.add(reset);//add reset button to buttonPanel 
		buttonPanel.add(submit);//add submit button to buttonPanel
		
		//for adding the buttonPanel to the maingrid
		c.gridx=0; c.gridy=9;//first column tenth row (0,9)
		c.gridwidth=2;//this spans two columns
		c.anchor = GridBagConstraints.CENTER;//center the buttons
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(15, 10, 5, 10);//add some extra padding
		mainGrid.add(buttonPanel,c);//add buttonPanel to mainGrid
		
		////not necessary but this changes c back to normal
		//c.gridwidth=1;//go back to only covering one column
		//c.anchor = GridBagConstraints.WEST;//go back to left aligning
		//c.insets = new Insets(5, 10, 5, 10); //set the padding back to normal
		
		this.add(mainGrid);//add the contents to the tab
		reset.addMouseListener(new mouseListener());
		submit.addMouseListener(new mouseListener());
		
		
		
	}
	
	private class mouseListener implements MouseListener {
		public void mouseReleased(MouseEvent e) {}
		public void mousePressed(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseClicked(MouseEvent e) {
			if(e.getSource() == reset){
				//make all fields blank
				idNumber.setText("");
				firstName.setText("");
				lastName.setText("");
				phoneNumber.setText("");
				email.setText("");
				address.setText("");
				city.setText("");
				state.setText("");
				zip.setText("");
			}else if(e.getSource()==submit){
				//add the new user to db
						String sql = 
						"insert into patron (p_ID, firstName, lastName, phoneNumber, " +
						"email, address, city, state, zip) "
						+"values (" +
						""		+idNumber.getText()+
						",'"	+firstName.getText()+
						"','"	+lastName.getText()+
						"','"	+phoneNumber.getText()+
						"','"	+email.getText()+
						"','"	+address.getText()+
						"','"	+city.getText()+
						"','"	+state.getText()+
						"','"	+zip.getText()+
								"');";
				//System.out.println(sql);
				database.updateDatabase(sql);
				idNumber.setText("");
				firstName.setText("");
				lastName.setText("");
				phoneNumber.setText("");
				email.setText("");
				address.setText("");
				city.setText("");
				state.setText("");
				zip.setText("");
				
			}
		}
	}
	
}
