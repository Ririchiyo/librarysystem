import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;


public class database {
    public static Connection conn;
	public static final String dbName = "librarySystem.db";

    

	public static boolean createConnection() {
		try{
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:librarySystem.db");
		return true;
		}catch(Exception e){
			System.out.println("Error conecting to database: "+e.getMessage());
			return false;
		}
		//return conn;
	}
	
	public static Vector<Vector<String>> getDatabaseInfo(String sql) {
		Vector<String> v = new Vector<String>();
		Vector<Vector<String>> Info = new Vector<Vector<String>>();
		if(createConnection()) {
			try {
		        Statement stat = conn.createStatement();
		        ResultSet rs = stat.executeQuery(sql);
				ResultSetMetaData rsMetaData = rs.getMetaData();
				
				while(rs.next()) { // moves row cursor
					v=new Vector<String>();
					
					for(int c=1;c <= rsMetaData.getColumnCount(); c++) { // moves column cursor 
						v.add(rs.getString(c)); // adds to vector string
					}
					Info.add(v); // adds to vector vector string
				}
				return Info;
			} catch(Exception e) {
				System.out.println(e.getMessage());
				return Info;
			}
		}
		else
			return Info;
		
	}
	
	public static boolean dropDatabase() {
		if(createConnection()) {
			try {
				Statement stat = conn.createStatement();
				stat.executeUpdate("DROP TABLE IF EXISTS material");
				stat.executeUpdate("DROP TABLE IF EXISTS patron");
				stat.executeUpdate("DROP TABLE IF EXISTS rent");
				closeConnection();
					return true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
				closeConnection();
					return false;
			}
		} else {
			closeConnection();
				return false;
		}
	}
	
	public static boolean updateDatabase(String sql) {
		if(createConnection()) {
			try {
				Statement stat = conn.createStatement();
				stat.executeUpdate(sql);
					closeConnection();
						return true;
				}catch(Exception e) {
					System.out.println(e.getMessage());
					closeConnection();
						return false;
				}
		}
		else {
			closeConnection();
				return false;
		}
	}
	
	public static boolean closeConnection(){
		try{
			conn.close();
			return true;
		}catch(Exception e){
			System.out.println("Error closing connection: "+ e.getMessage());
			return false;
		}
		
		
	}
}
