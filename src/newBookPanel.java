import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class newBookPanel extends JPanel {
	
	private final JTextField  title 			;
	private final JTextField  format 		    ;
	private final JTextField  author 		    ;
	private final JTextField  ISBN 			;
	private final JTextField  yearPublished 	;
	private final JTextField  publisher 		;
	private final JTextField  callNumber 	    ;
	private final JTextField  copyNumber 	    ;
	private final JButton 		reset			;
	private final JButton 		submit			;
	
	
	public newBookPanel(){
		JPanel mainGrid = new JPanel();//panel that will hold all stuff in tab
		JPanel buttonPanel = new JPanel();//panel for holding buttons
		
		mainGrid.setLayout(new GridBagLayout());//the panel is a grid
		GridBagConstraints c = new GridBagConstraints();//holds properties for cells when added into grid
		
		//1 row, 2 columns, 15 horizontal padding, 0 vertical padding
		buttonPanel.setLayout(new GridLayout(1,2,15,0));
		
		c.insets = new Insets(5, 10, 5, 10);//add some padding around the elements added
		c.anchor = GridBagConstraints.WEST;//align left
		c.fill = GridBagConstraints.HORIZONTAL;
		
		//create labels so the user knows what goes into the text box that will be to the right
		JLabel titleLabel 			= new JLabel("Title :");
		JLabel formatLabel 			= new JLabel("Format :");
		JLabel authorLabel 			= new JLabel("Author :");
		JLabel ISBNLabel 			= new JLabel("ISBN :");
		JLabel yearPublishedLabel 	= new JLabel("Year Published (Integer) :");
		JLabel publisherLabel 		= new JLabel("Publisher :");
		JLabel callNumberLabel 		= new JLabel("Call Number (Integer) :");
		JLabel copyNumberLabel 		= new JLabel("Copy Number (Integer) :");

		//create input boxes for:
		 title 			= new JTextField(25);
		 format 		= new JTextField();
		 author 		= new JTextField();
		 ISBN 			= new JTextField();
		 yearPublished 	= new JTextField();
		 publisher 		= new JTextField();
		 callNumber 	= new JTextField();
		 copyNumber 	= new JTextField();
		
		reset 	= new JButton("Reset Fields");//button for reseting input
		submit 	= new JButton("Submit");

		c.gridx=0;	c.gridy=0;	//set cursor to  first column first row
		
		//add the following labels to grid, move cursor to next row after each insert
		mainGrid.add(titleLabel, c); 			c.gridy++;//1
		mainGrid.add(formatLabel, c);			c.gridy++;//2
		mainGrid.add(authorLabel, c);			c.gridy++;//3
		mainGrid.add(ISBNLabel, c);				c.gridy++;//4
		mainGrid.add(yearPublishedLabel, c);	c.gridy++;//5
		mainGrid.add(publisherLabel, c);		c.gridy++;//6
		mainGrid.add(callNumberLabel, c);		c.gridy++;//7
		mainGrid.add(copyNumberLabel, c);		c.gridy++;//8
		
		c.gridx=1;	c.gridy=0;	//set cursor to second column, first row
		
		//add the following text boxes to grid, move cursor to next row after each insert
		mainGrid.add(title, c); 			c.gridy++;//1
		mainGrid.add(format, c);			c.gridy++;//2
		mainGrid.add(author, c);			c.gridy++;//3
		mainGrid.add(ISBN, c);				c.gridy++;//4
		mainGrid.add(yearPublished, c);		c.gridy++;//5
		mainGrid.add(publisher, c);			c.gridy++;//6
		mainGrid.add(callNumber, c);		c.gridy++;//7
		mainGrid.add(copyNumber, c);		c.gridy++;//8
		
		//adding to a GridLayout dosen't require constraints(c), GridBagLayout does
		buttonPanel.add(reset);//add reset button to buttonPanel 
		buttonPanel.add(submit);//add submit button to buttonPanel
		
		//for adding the buttonPanel to the mainGrid
		c.gridx=0; c.gridy=8;//first column tenth row (0,9)
		c.gridwidth=2;//this spans two columns
		c.anchor = GridBagConstraints.CENTER;//center the buttons
		c.insets = new Insets(15, 10, 5, 10);//add some extra padding
		mainGrid.add(buttonPanel,c);//add buttonPanel to mainGrid
		
		////not necessary but this changes c back to normal
		//c.gridwidth=1;//go back to only covering one column
		//c.anchor = GridBagConstraints.WEST;//go back to left aligning
		//c.insets = new Insets(5, 10, 5, 10); //set the padding back to normal
		
		this.add(mainGrid);//add the contents to the tab

		//buttonPanel.setBounds(0, 0, getWidth(), 35); dosent work
		reset.addMouseListener( new mouseListener());
		submit.addMouseListener(new mouseListener());
		
		
		
	}
	private class mouseListener implements MouseListener{
		public void mouseReleased(MouseEvent e) {}
		public void mousePressed (MouseEvent e) {}
		public void mouseExited  (MouseEvent e) {}
		public void mouseEntered (MouseEvent e) {}
		public void mouseClicked (MouseEvent e) {
			if(e.getSource()==reset){
				//make all fields blank
				title.setText("");
				format.setText("");
				author.setText("");
				ISBN.setText("");
				yearPublished.setText("");
				publisher.setText("");
				callNumber.setText("");
				copyNumber.setText("");
			}else if(e.getSource()==submit){
				String sql = 
						"insert into material (title, format, author, ISBN, " +
						"yearPublished, publisher, callNumber, copyNumber) "
						+"values (" +
						"'"		+title.getText()+
						"','"	+format.getText()+
						"','"	+author.getText()+
						"','"	+ISBN.getText()+
						"',"	+yearPublished.getText()+
						",'"	+publisher.getText()+
						"',"	+callNumber.getText()+
						","		+copyNumber.getText()+
								");";
				//System.out.println(sql);
				database.updateDatabase(sql);
				title.setText("");
				format.setText("");
				author.setText("");
				ISBN.setText("");
				yearPublished.setText("");
				publisher.setText("");
				callNumber.setText("");
				copyNumber.setText("");
			}
		}
	}
}
