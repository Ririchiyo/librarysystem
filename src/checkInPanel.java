import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;


public class checkInPanel extends JPanel {

	private static JTable userTable;
	private static JTable bookTable;
	private static DefaultTableModel userTableModel = new DefaultTableModel(){

		public boolean isCellEditable(int row, int column){return false;}
	};
	private static DefaultTableModel bookTableModel = new DefaultTableModel(){


		public boolean isCellEditable(int row, int column){return false;}
	};
	private JScrollPane scrollUserTable, scrollBookTable;
	private JLabel userLabel, bookLabel;
	private static int selectedUser=-1;
	private static int selectedBook=-1;
	private JButton refresh, submit;
	
	private final static int uTable_pID 		= 0;

	private final int bTable_CPn 		= 4; 
	private final int bTable_mID		=0;
	
	
	private int get_mID() {
		//return Integer.parseInt((String) bookTable.getValueAt(bookTable.getSelectedRow(), bTable_mID));
		//^^ since we have a hidden row in the book table we cannot get the hidden data be referencing the table
		
		//vv grab the data Vector which still has the hidden data in it
		Vector<Vector<String>> data = bookTableModel.getDataVector();// ***
		
		//data.elementAt(  /* the row I want */  ).elementAt(  /* the column I want */  );
		return Integer.parseInt(data.elementAt(bookTable.getSelectedRow()).elementAt(bTable_mID)); // ***
		
		// the 2 lines with *** next to them will need to be used whenever you get the hidden m_ID from the book table
		// since column 0 is hidden from the table view, when you use getValueAt(r,c)
		//		the first column shown is column 0! (title)
		
	}
	private static int get_pID() {
		return Integer.parseInt((String) userTable.getValueAt(userTable.getSelectedRow(), uTable_pID));
	}

	private String get_CP() {
		return bookTable.getValueAt(bookTable.getSelectedRow(), bTable_CPn).toString();
	}
		
	public checkInPanel(){
		
		JPanel mainGrid = new JPanel();//panel that will hold all stuff in tab
		JPanel buttonPanel = new JPanel();//panel for holding buttons
		
		mainGrid.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0; c.gridy=0; //first row first column
		c.anchor = GridBagConstraints.WEST;//align left
		c.fill = GridBagConstraints.HORIZONTAL;
		
		//1 row, 2 columns, 15 horizontal padding, 0 vertical padding
		buttonPanel.setLayout(new GridLayout(1,2,15,0));
		
		//labels to describe what the user has to do
		userLabel = new JLabel("User :");
		bookLabel = new JLabel("Material ID :");
		//JLabel dateLabel = new JLabel("Date :");
		
		refresh = new JButton("Refresh Tables");
		submit = new JButton("Check In");
		
		bookTable = new JTable(bookTableModel);
		bookTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		userTable = new JTable(userTableModel);
		userTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Dimension d = new Dimension(500, 120);
		
		scrollBookTable = new JScrollPane(bookTable); 
		scrollBookTable.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS); 
		scrollBookTable.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 
		scrollBookTable.setMaximumSize(d);
		scrollBookTable.setMinimumSize(d);
		scrollBookTable.setPreferredSize(d);
		
		scrollUserTable = new JScrollPane(userTable); 
		scrollUserTable.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS); 
		scrollUserTable.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 
		scrollUserTable.setMaximumSize(d);
		scrollUserTable.setMinimumSize(d);
		scrollUserTable.setPreferredSize(d);
		
		buttonPanel.add(refresh);
		buttonPanel.add(submit);
		
		
		updateUserTable();
		updateBookTable();
		
		mainGrid.add(userLabel, c);						c.gridy++;
		mainGrid.add(scrollUserTable, c);				c.gridy++;
		c.insets = new Insets(15, 0, 0, 0);//put some padding between users and books
		mainGrid.add(bookLabel, c);						c.gridy++;
		c.insets = new Insets(0,0,0,0);//back to no padding
		mainGrid.add(scrollBookTable, c);				c.gridy++;
		c.insets = new Insets(15, 0, 0, 0);//put some padding between books and date

		mainGrid.add(buttonPanel, c);						c.gridy++;
		
		this.add(mainGrid);//add contents to tab
		
		
		userTable.addMouseListener(new mouseListener());
		bookTable.addMouseListener(new mouseListener());
		refresh.addMouseListener(new mouseListener());
		submit.addMouseListener(new mouseListener());
		
	}
	
	public class mouseListener implements MouseListener{
		@Override
		public void mouseClicked(MouseEvent e) {}
		@Override
		public void mouseEntered(MouseEvent e) {}
		@Override
		public void mouseExited(MouseEvent e) {}
		@Override
		public void mousePressed(MouseEvent e) {}
		@Override
		public void mouseReleased(MouseEvent e) {
			if(e.getSource()==userTable){
				selectedUser=userTable.getSelectedRow();
				userLabel.setText("User :  "+ (selectedUser!=-1 ? get_pID() : ""));
				updateBookTable();
				//don't know why i should need to use this, but it makes it work
				//checkInPanel.this.paintImmediately(checkInPanel.this.getVisibleRect());
				

			}else if(e.getSource()==bookTable){
				selectedBook = bookTable.getSelectedRow();
				bookLabel.setText("Material ID :  "+ (selectedBook!=-1 ? (selectedBook+1) : ""));
				
				//don't know why i should need to use this, but it makes it work
				//checkInPanel.this.paintImmediately(checkInPanel.this.getVisibleRect());
			}
			else if(e.getSource()==refresh) {
				updateUI();
			}
			else if(e.getSource()==submit) {
				if(selectedUser != -1 && selectedBook != -1) {
					String sql;
					sql = "delete from rent where m_copyNumber = " + get_CP() + " AND p_ID = " + get_pID() + " AND m_ID = " + get_mID();
					//System.out.println(sql);
					database.updateDatabase(sql);
					updateBookTable();
				}
			}
		}
	}
	
	private void updateTables(){
		updateBookTable();
		updateUserTable();

	}
		
	private static void updateBookTable(){
		bookTableModel.setDataVector(fillBookData(selectedUser), fillBookColNames());
		bookTable.getColumnModel().removeColumn( bookTable.getColumnModel().getColumn(0));

	}
	static void updateUserTable(){
		userTableModel.setDataVector(fillUserData(), fillUserColNames());
	}
	

	private static Vector<String> fillUserColNames(){
		Vector<String> colNames = new Vector<String>();
		colNames.add("ID");
		colNames.add("First");
		colNames.add("Last");
		colNames.add("Phone");
		colNames.add("Email");
		colNames.add("Address");
		colNames.add("City");
		colNames.add("State");
		colNames.add("Zip");		
		return colNames;
	}
	private static Vector<String> fillBookColNames(){
		Vector<String> colNames = new Vector<String>();
		colNames.add("ID");
		colNames.add("Title");
		colNames.add("Format");
		colNames.add("Author");
		colNames.add("ISBN");
		//colNames.add("Year Pub.");
		//colNames.add("Publisher");
		//colNames.add("Call #");
		colNames.add("Copy #");
		colNames.add("Due Date");
		return colNames;	
	}
	private static Vector<Vector<String>> fillUserData(){
		Vector<Vector<String>> v = new Vector<Vector<String>>();
		if(database.createConnection()) {
			String sql;
			Vector<Vector<String>> Info;
			sql = "Select * from patron";
				Info = database.getDatabaseInfo(sql);
				return Info;
		}
		return v;
	}
	
	private static Vector<Vector<String>> fillBookData( int user) {

		Vector<Vector<String>> Info = new Vector<Vector<String>>();
		if(selectedUser > -1){
			if(database.createConnection()) {
				String sql;
				sql = "select distinct material.m_ID, material.title, material.format, material.author, material.ISBN, material.copyNumber, rent.due from rent, " +
						" material, patron where rent.m_ID=material.m_ID AND rent.p_ID="+get_pID();
				//System.out.println(get_pID());
				Info = database.getDatabaseInfo(sql);
				return Info;
			}
		}
		return Info;
	}
}
