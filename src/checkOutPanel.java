import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;


public class checkOutPanel extends JPanel {
	
	private JTable userTable;
	private static JTable bookTable;
	private JScrollPane scrollUserTable, scrollBookTable;
	private JLabel userLabel, bookLabel;
	private static DefaultTableModel userTableModel = new DefaultTableModel(){

		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	private static DefaultTableModel bookTableModel = new DefaultTableModel() {
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	private int selectedUser = -1;
	private int selectedBook = -1;
	private JComboBox date_month, date_day, date_year;
	private JButton refresh, submit;
	
	
		//User Table
	private final int uTable_pID 		= 0;
		
		//Book Table
	private final int bTable_mID 		= 0; // hidden column
	private final int bTable_Title 	= 1; 
	private final int bTable_ISBN 	= 3; 
	private final int bTable_CPn 		= 7; 

	
	private int get_pID() {
		return Integer.parseInt((String) userTable.getValueAt(userTable.getSelectedRow(), uTable_pID));
	}
	
	private int get_mID() {
		//return Integer.parseInt((String) bookTable.getValueAt(bookTable.getSelectedRow(), bTable_mID));
		//^^ since we have a hidden row in the book table we cannot get the hidden data be referencing the table
		
		//vv grab the data Vector which still has the hidden data in it
		Vector<Vector<String>> data = bookTableModel.getDataVector();// ***
		
		//data.elementAt(  /* the row I want */  ).elementAt(  /* the column I want */  );
		return Integer.parseInt(data.elementAt(bookTable.getSelectedRow()).elementAt(bTable_mID)); // ***
		
		// the 2 lines with *** next to them will need to be used whenever you get the hidden m_ID from the book table
		// since column 0 is hidden from the table view, when you use getValueAt(r,c)
		//		the first column shown is column 0! (title)
		
	}

	private String get_CP() {
		return bookTable.getValueAt(bookTable.getSelectedRow(), bTable_CPn).toString();
	}
	
	public checkOutPanel(){
		
		JPanel mainGrid = new JPanel();//panel that will hold all stuff in tab
		JPanel buttonPanel = new JPanel();//panel for holding buttons
		JPanel footer = new JPanel();
		
		mainGrid.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0; c.gridy=0; //first row first column
		c.anchor = GridBagConstraints.WEST;//align left
		c.fill = GridBagConstraints.HORIZONTAL;
		
		//1 row, 2 columns, 15 horizontal padding, 0 vertical padding
		buttonPanel.setLayout(new GridLayout(1,2,15,0));
		footer.setLayout(new GridLayout(1,2,15,0));
		
		//labels to describe what the user has to do
		userLabel = new JLabel("User :  ");
		bookLabel = new JLabel("Material ID :  ");
		JLabel dateLabel = new JLabel("Date :  ");
		JPanel datePanel = new JPanel();
		
		bookTable = new JTable(bookTableModel);
		bookTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		userTable = new JTable(userTableModel);
		userTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Dimension d = new Dimension(500, 120);
		
		scrollBookTable = new JScrollPane(bookTable); 
		scrollBookTable.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS); 
		scrollBookTable.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 
		scrollBookTable.setMaximumSize(d);
		scrollBookTable.setMinimumSize(d);
		scrollBookTable.setPreferredSize(d);
		
		scrollUserTable = new JScrollPane(userTable); 
		scrollUserTable.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS); 
		scrollUserTable.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 
		scrollUserTable.setMaximumSize(d);
		scrollUserTable.setMinimumSize(d);
		scrollUserTable.setPreferredSize(d);
		
		updateTables();
		
		//String[] monthList = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};		
		
		Vector<Integer> month = new Vector<Integer>();
		for (int i=1; i<=12; i++) { month.add(i); }
		
		Vector<Integer> days = new Vector<Integer>();
		for (int i=1 ; i<=31 ; i++) { days.add(i); }
		
		Vector<Integer> years = new Vector<Integer>();
		for (int i=2099 ; i>=2012 ; i--) { years.add(i); }
		
		date_month = new JComboBox(month);
		date_day = new JComboBox(days);
		date_year = new JComboBox(years); date_year.setSelectedIndex(years.size()-1);
		refresh = new JButton("Refresh Tables");
		submit = new JButton("Check Out");
		
		buttonPanel.add(refresh);
		buttonPanel.add(submit);
		datePanel.add(dateLabel);
		datePanel.add(date_month);
		datePanel.add(date_day);
		datePanel.add(date_year);
		
		footer.add(datePanel);
		footer.add(buttonPanel);
		
		
		
		mainGrid.add(userLabel, c);						c.gridy++;
		mainGrid.add(scrollUserTable, c);				c.gridy++;
		c.insets = new Insets(15, 0, 0, 0);//put some padding between users and books
		mainGrid.add(bookLabel, c);						c.gridy++;
		c.insets= new Insets(0,0,0,0);//back to no padding
		mainGrid.add(scrollBookTable, c);				c.gridy++;
		c.insets = new Insets(15, 0, 0, 0);//put some padding between books and date

		mainGrid.add(footer, c);						c.gridy++;
		this.add(mainGrid);//add contents to tab
		
		
		userTable.addMouseListener(new mouseListener());
		bookTable.addMouseListener(new mouseListener());
		refresh.addMouseListener(new mouseListener());
		submit.addMouseListener(new mouseListener());
		
	}
	
	public class mouseListener implements MouseListener{
		
		@Override
		public void mouseClicked(MouseEvent e) {}
		@Override
		public void mouseEntered(MouseEvent e) {}
		@Override
		public void mouseExited(MouseEvent e) {}
		@Override
		public void mousePressed(MouseEvent e) {}
		@Override
		public void mouseReleased(MouseEvent e) {
			//		return Integer.parseInt(data.elementAt(bookTable.getSelectedRow()).elementAt(bTable_mID)); // ***

			if(e.getSource()==userTable){
				Vector<Vector<String>> data = userTableModel.getDataVector();// ***

				selectedUser = Integer.parseInt(data.elementAt(userTable.getSelectedRow()).elementAt(uTable_pID));
				//selectedUser=userTable.getSelectedRow();
				userLabel.setText("User :  "+ (selectedUser!=-1 ? selectedUser : ""));
				
				//don't know why i should need to use this, but it makes it work
				//checkOutPanel.this.paintImmediately(checkOutPanel.this.getVisibleRect());
				
				
			}else if(e.getSource()==bookTable){
				selectedBook = bookTable.getSelectedRow();
				//System.out.println("Date Month : "+date_month.getSelectedItem().toString());
				bookLabel.setText("Material ID :  "+ (selectedBook!=-1 ? (selectedBook+1) : ""));
				
				//don't know why i should need to use this, but it makes it work
				//checkOutPanel.this.paintImmediately(checkOutPanel.this.getVisibleRect());
			}
			else if(e.getSource()==refresh){
				updateTables();
				selectedUser = -1;
				selectedBook = -1;
			}else if(e.getSource()==submit){
				

				if(selectedUser != -1 && selectedBook != -1) {
					
					int year= (Integer) date_year.getItemAt(date_year.getSelectedIndex());
					int month = date_month.getSelectedIndex()+1;
					int day = date_day.getSelectedIndex()+1;
					//System.out.println("Year: "+year+"\nMonth: "+month+"\nDay :"+day);
					int date = (year*10000)+(month*100)+day; 
				String sql = 
						"insert into rent (p_ID, m_ID, m_copyNumber, due)"
						+"values (" +
								+get_pID()+
								","+get_mID()+
								","+get_CP()+
								","+date+
						");";
					//System.out.println(sql);
				database.updateDatabase(sql);
				updateTables();
				selectedUser = -1;
				selectedBook = -1;
				}
			}
		}
	}
		
		
	
	
	
	public static void updateTables(){
		bookTableModel.setDataVector(fillBookData(), fillBookColNames());
		userTableModel.setDataVector(fillUserData(), fillUserColNames());
		
		
		bookTable.getColumnModel().removeColumn( bookTable.getColumnModel().getColumn(0));
		// ^^^ this will remove the first column from the column model
		//     this will only prevent the column from being displayed
		//     it does not remove any data from the data Vector
	}
	

	

	private static Vector<String> fillUserColNames(){
		Vector<String> colNames = new Vector<String>();
		colNames.add("ID");
		colNames.add("First");
		colNames.add("Last");
		colNames.add("Phone");
		colNames.add("Email");
		colNames.add("Address");
		colNames.add("City");
		colNames.add("State");
		colNames.add("Zip");		
		return colNames;
	}
	private static Vector<String> fillBookColNames(){
		Vector<String> colNames = new Vector<String>();
		colNames.add("ID");
		colNames.add("Title");
		colNames.add("Format");
		colNames.add("Author");
		colNames.add("ISBN");
		colNames.add("Year Pub.");
		colNames.add("Publisher");
		colNames.add("Call #");
		colNames.add("Copy #");		
		return colNames;	
	}
	private static Vector<Vector<String>> fillUserData(){
		Vector<Vector<String>> v = new Vector<Vector<String>>();
		//Vector<String> u;
		if(database.createConnection()) {
			String sql;
			Vector<Vector<String>> Info;
			sql = "Select * from patron";
			Info = database.getDatabaseInfo(sql);
			return Info;
		}
		return v;
	}
	
	private static Vector<Vector<String>> fillBookData() {
		Vector<Vector<String>> v = new Vector<Vector<String>>();
		//Vector<String> b;
		if(database.createConnection()) {
			String sql;
			Vector<Vector<String>> Info;
			sql = "Select * from material where m_ID NOT IN (select m_id from rent)";
			Info = database.getDatabaseInfo(sql);
			return Info;
		}
		return v;
	}
}
