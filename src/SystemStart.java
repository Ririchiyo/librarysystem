import java.sql.Statement;

public class SystemStart{
	final static int debug = 0; // to be used for actual debugging functions later
	private SystemStart() {
		try {
			if(database.createConnection()) {
					Statement stat = database.conn.createStatement();
						stat.executeUpdate(
								"CREATE TABLE IF NOT EXISTS material ("+
									"m_ID INTEGER not null PRIMARY KEY AUTOINCREMENT,"+
									"title text not null,"+
									"format text null,"+
									"author text null,"+
									"ISBN text not null,"+
									"yearPublished INTEGER null,"+
									"publisher text null," +
									"callNumber INTEGER null," +
									"copyNumber INTEGER null" +
								");"
						);
						stat.executeUpdate(
								"CREATE TABLE IF NOT EXISTS patron ("+
									"p_ID INTEGER not null PRIMARY KEY," +
									"firstName text null,"+
									"lastName text null,"+
									"phoneNumber text null,"+
									"email text null,"+
									"address text null,"+
									"city text null,"+
									"state text null,"+
									"zip text null"+
									");"		
						);
						stat.executeUpdate(
								"CREATE TABLE IF NOT EXISTS rent ("+
								"r_ID INTEGER not null PRIMARY KEY AUTOINCREMENT," +
								"p_ID INTEGER not null," +
								"m_ID INTEGER not null,"+
								"m_copyNumber not null,"+
								"due INTEGER null,"+
								"CONSTRAINT rentFK1 FOREIGN KEY(p_ID) REFERENCES patron(p_ID),"+
								"CONSTRAINT rentFK2 FOREIGN KEY(m_ID) REFERENCES material(m_ID)"+
								");"
						);
						if(database.closeConnection()) {
							//This should run successfully, so there is no need to have anything in here
						} else {
							System.out.println("There was an error closing the database");
						}
				}
			else
				System.out.println("There was an error connecting to the database");
			} catch (Exception e ) {
				System.out.println(e.getMessage());
			}
	}
	    	
	public static void main(String[] args) {

		new SystemStart();
		new TabbedFrame("Libary System");
	}
}
