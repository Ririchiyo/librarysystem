import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;


public class TabbedFrame extends JFrame{
		
	public TabbedFrame(String frameTitle){
		final JTabbedPane tabPanels = new JTabbedPane();//name the tabbed component
		
		final JComponent userPanel = new newUserPanel();//make the user panel
		//make the first tab with the panel above
		 tabPanels.addTab("New Patron", null, userPanel,"Add a new patron to the library.");

		final JComponent bookPanel = new newBookPanel();//make the new book panel
		//make the second tab with the panel above
		tabPanels.addTab("New Material", null, bookPanel,"Add new material to the library.");

		final JComponent outPanel = new checkOutPanel();
		tabPanels.addTab("Check Out", null, outPanel,
		                  "Check out material.");

		final JComponent inPanel = new checkInPanel();
		tabPanels.addTab("Check In", null, inPanel,
		                      "Check in material.");
		
		final JComponent overduePanel = new overdueBooksPanel();
		overduePanel.setPreferredSize(new Dimension(500, 360));//used to set the size of the tabbed component,will stretch to fit other components
		tabPanels.addTab("Overdue Material", null, overduePanel,
		                      "List overdue material.");
		
		this.add(tabPanels);
		this.pack();
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
//		tabPanels.addChangeListener(new ChangeListener() {
//			
//			@Override
//			public void stateChanged(ChangeEvent e) {
//				int count = tabPanels.getTabCount();
//				for(int i = 0;i<count;i++){
//					  if(tabPanels.getBoundsAt(i).contains((e.getPoint()))){ //where e is the mouseEvent
//					    System.out.println("Mouse was clicked in tab bounds");
//					    return;
//					  }
//				}
//				
//			}
//		});
		
		tabPanels.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {
				int tabNumber=-1;
				int count = tabPanels.getTabCount();
				for(int i = 0;i<count;i++){
					  if(tabPanels.getBoundsAt(i).contains((e.getPoint()))){ //where e is the mouseEvent
					    //System.out.println(i);
					    tabNumber=i;
					  }
				}
				
				//do on click events with tab tabNumber
				
				//	Checkout
				if(tabNumber == 2 ) { 
					checkOutPanel.updateTables();
				}
				
				//	CheckIn
				if(tabNumber == 3){
					checkInPanel.updateUserTable();
				}
				if(tabNumber == 4) {
					overdueBooksPanel.updateTables();
				}
				
				
				
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
}
