import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;


public class overdueBooksPanel extends JPanel {

	
		private JScrollPane overdueBookTable;
		private JLabel overdueBookLabel;
		private static DefaultTableModel overdueBookTableModel = new DefaultTableModel(){
			public boolean isCellEditable(int row, int column){return false;}
		};
		private JButton  refresh = new JButton("Refresh Tables");
		
		
		
		public overdueBooksPanel(){
		
		JPanel mainGrid = new JPanel();//panel that will hold all stuff in tab
		JPanel buttonPanel = new JPanel();//panel for holding buttons
		
		mainGrid.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0; c.gridy=0; //first row first column
		c.anchor = GridBagConstraints.WEST;//align left
		c.fill = GridBagConstraints.HORIZONTAL;

		buttonPanel.setLayout(new GridLayout(1,1,15,0));

		overdueBookLabel = new JLabel("Overdue Material :");
		
		JTable oBT = new JTable(overdueBookTableModel);
		oBT.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Dimension d = new Dimension(500, 300);
		
		overdueBookTable = new JScrollPane(oBT); 
		overdueBookTable.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS); 
		overdueBookTable.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 
		overdueBookTable.setMaximumSize(d);
		overdueBookTable.setMinimumSize(d);
		overdueBookTable.setPreferredSize(d);
		
		buttonPanel.add(refresh);
		
		updateTables();
		
		mainGrid.add(overdueBookLabel, c);						c.gridy++;

		mainGrid.add(overdueBookTable, c);				c.gridy++;
		c.insets = new Insets(10, 10, 5, 10);//add some extra padding

		mainGrid.add(buttonPanel, c);							c.gridy++;
		
		this.add(mainGrid);
		
	}
	
	public class mouseListener implements MouseListener{
		
		@Override
		public void mouseClicked(MouseEvent e) {}
		@Override
		public void mouseEntered(MouseEvent e) {}
		@Override
		public void mouseExited(MouseEvent e) {}
		@Override
		public void mousePressed(MouseEvent e) {}
		@Override
		public void mouseReleased(MouseEvent e) {
			if(e.getSource()==refresh){
				updateTables();
			}
		}
	}
	
	static void updateTables(){
		overdueBookTableModel.setDataVector(fillOverdueBookData(), fillOverdueBookColNames());
	}
	
	private static Vector<String> fillOverdueBookColNames(){
		Vector<String> colNames = new Vector<String>();
		colNames.add("Patron");
		colNames.add("Title");
		colNames.add("Format");
		colNames.add("Author");
		colNames.add("ISBN");
		colNames.add("copyNumber");
		colNames.add("Date Due");
		//colNames.add("Copy #");		
		return colNames;	
	}
	private static Vector<Vector<String>> fillOverdueBookData() {
		Vector<Vector<String>> v = new Vector<Vector<String>>();
		if(database.createConnection()) {
			String sql;
			Vector<Vector<String>> Info;
			sql = "select distinct patron.p_ID, material.title, material.format, material.author, material.ISBN, material.copyNumber, rent.due from rent" +
					", material, patron where rent.m_ID=material.m_ID AND rent.p_ID=patron.p_ID AND rent.due < (select strftime('%Y%m%d', 'now'));";
			Info = database.getDatabaseInfo(sql);
			return Info;
		}
		return v;
	}
}
